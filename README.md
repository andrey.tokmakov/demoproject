# DemoProject

0. [Overview](#Overview)
1. [Download](#Download)
2. [Build](#Build)
3. [Run application](#Run)
4. [Tests ](#Tests)



<a name="Overview"></a> 
## Overview
Demo version of the Client-Server application and its integration tests

<a name="Download"></a> 
## Download project:
- git clone https://gitlab.com/andrey.tokmakov/demoproject.git  
- cd demoproject

<a name="Build"></a> 
## Hot to build project:
- mkdir build
- cd build
- cmake ..
- make all

<a name="Run"></a> 
## Run:
To run server execute command `./src/server` from the _build_ directory  
To run client execute command `./src/client` from the _build_ directory  

<a name="Tests"></a> 
## How to run tests
Project include both integration and performance tests (in the form of a small demonstration).   
Execute command `./test/tests` from the _build_ directory  
