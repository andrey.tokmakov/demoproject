#include "server.h"

#include <iostream>
#include <cstring>
#include <signal.h>

UnixSockServer::UnixSockServer() {
	instancePtr = this;
}

UnixSockServer::~UnixSockServer() {
	shutdown();
}

void UnixSockServer::shutdown() noexcept {
	::close(m_socketConection);
    ::close(m_socketServer);
    m_socketConection = INVALID_SOCKET;
    m_socketServer = INVALID_SOCKET;
    unlink(m_local.sun_path);
}

bool UnixSockServer::init(std::string_view sockPath) {
	m_socketServer = ::socket(AF_UNIX, SOCK_STREAM, 0);
    if (INVALID_SOCKET == m_socketServer) {
        std::cerr << "Failed to create server socket. Error = " << errno << "\n";
        return false;
    }

    m_local.sun_family = AF_UNIX;
    std::strncpy(m_local.sun_path, sockPath.data(), sizeof(m_local.sun_path) - 1);

    unlink(m_local.sun_path);
    const size_t length = std::strlen(m_local.sun_path) + sizeof(m_local.sun_family);

    if (SOCKET_ERROR == ::bind(m_socketServer, reinterpret_cast<sockaddr*>(&m_local), static_cast<socklen_t>(length))) {
        std::cerr << "Failed to bind server socket. Error = " << errno << "\n";
        return false;
    }

    if (SOCKET_ERROR == ::listen(m_socketServer, 1)) {
        std::cerr << "Failed to listen server socket. Error = " << errno << "\n";
        return false;
    }
    return true;
}

bool UnixSockServer::accepted(){
    unsigned int sizeRemote = sizeof(m_remote);
    if (m_socketConection > 0) 
		return true;
	m_socketConection = ::accept(m_socketServer, reinterpret_cast<sockaddr*>(&m_remote), &sizeRemote);
    if (INVALID_SOCKET == m_socketConection){
        ::close(m_socketConection);
        m_socketConection = INVALID_SOCKET;
        return false;
    }
	
	timeval tv {SOCKET_READ_TIMEOUT, 0};
	setsockopt(m_socketConection, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
	
    return true;
}

// TODO: implement calls to other service functions, to handle different signals.
void UnixSockServer::dispatch(int signal) noexcept {
	if (SIGUSR1 == signal) {
		printConnectionState();
	} else {
		std::cout << "Signal " << signal << " received. Shutting down server...\n";
		exit(signal);
	}
}

void UnixSockServer::printConnectionState() const noexcept {
	if (INVALID_SOCKET != m_socketConection) {
		std::cout << "Server is busy. Client connected.\n";
	} else {
		std::cout << "Server is available\n";
	}
}

