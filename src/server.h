#pragma once

#include <string>
#include <string_view>
#include <sys/un.h>
#include <sys/socket.h>
#include <unistd.h>
#include <chrono>

#define INVALID_SOCKET    -1
#define SOCKET_ERROR      -1

class UnixSockServer final {
public:
    UnixSockServer();
    ~UnixSockServer();

    UnixSockServer(UnixSockServer &) = delete;
    UnixSockServer(UnixSockServer &&) = delete;
    UnixSockServer& operator=(UnixSockServer&) = delete;
    UnixSockServer& operator=(UnixSockServer&&) = delete;

    bool init(std::string_view sockPath);
	
    void step(auto func){
            std::string message;
            char str[2] = {0};
			const std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
			std::chrono::duration<size_t> timeElapsed {0};
            while (str[0] != '\n') {
                const int recvCount = ::recv(m_socketConection, str, 1, 0);
				const auto end = std::chrono::steady_clock::now();
				timeElapsed = std::chrono::duration_cast<std::chrono::duration<size_t>>(end - start);
				
				// 1. Check if connection is closed by the remote host
				// 2. or we have hit the TIMEOUT
				// Than we should close the client connection
                if (recvCount == 0 || timeElapsed.count() >= step_max_time) {
                    ::close(m_socketConection);
                    m_socketConection = INVALID_SOCKET;
                    return;
                }
                str[recvCount] = 0;
                message.append(str);
            }
			
			// Remove '\n'. We don't need it here since 
			// it is just a part of the protocol/contract 
			message.resize(message.size() - 1);
            func(std::move(message));
    }

    bool accepted();
	
	static inline void handle_signal(int signal) noexcept {
		instancePtr->dispatch(signal);
	}
	
	inline void setStepTimeout(size_t timeout) noexcept {
		step_max_time = timeout;
	}
	
	void shutdown() noexcept;
	
protected:
    void dispatch(int signal) noexcept;
	void printConnectionState() const noexcept;
	
private:
	size_t step_max_time { 60 };
    int m_socketServer {INVALID_SOCKET};
    int m_socketConection {INVALID_SOCKET};
    sockaddr_un m_local;
    sockaddr_un m_remote;
	
	static inline constexpr unsigned int SOCKET_READ_TIMEOUT { 1};
	static inline UnixSockServer* instancePtr {nullptr};
};
