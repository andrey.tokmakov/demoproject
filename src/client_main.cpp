
#include "client.h"
#include <iostream>

namespace {
	// TODO: Redefine with appropriate and accessible file path
	constexpr std::string_view socket_path {"/tmp/handle.sock"};
}

int main(int argc, char** params) 
{
	UnixSockClient socket(socket_path);
	if (false == socket.init())
		return 1;
		
	std::string input;
	while (true) {
		std::cout << "Enter text: ";
		std::getline(std::cin, input);
		socket.send(std::move(input));
	}
}