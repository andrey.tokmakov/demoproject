#include "server.h"

#include <iostream>
#include <cstring>
#include <signal.h>

namespace {
	// TODO: Redefine with appropriate and accessible file path
	constexpr std::string_view socket_path {"/tmp/handle.sock"};
}

int main(int argc, char** params) 
{
	UnixSockServer server;
	if (false == server.init(socket_path))
		return 1;
	
	signal(SIGINT,  UnixSockServer::handle_signal);  
	signal(SIGUSR1, UnixSockServer::handle_signal);
		
	while (true) {
		if (server.accepted()) {
			server.step([](auto&& data) {
				std::cout << "Message from client: " << data << "\n";
			});
		}
	}
}
