
#include "client.h"

#include <iostream>
#include <cstring>
#include <sys/socket.h>
#include <unistd.h>

UnixSockClient::UnixSockClient(std::string_view sockPath){
	memset(&m_remote, 0, sizeof(struct sockaddr_un));
    m_remote.sun_family = AF_UNIX;
    std::strncpy(m_remote.sun_path, sockPath.data(), sizeof(m_remote.sun_path)-1);
    m_len = std::strlen(m_remote.sun_path) + sizeof(m_remote.sun_family);
}

UnixSockClient::~UnixSockClient(){
    ::close(m_sock);
}

void UnixSockClient::close(){
    if (m_sock > 0) {
        ::close(m_sock);
        m_sock = INVALID_SOCKET;
    }
}

bool UnixSockClient::init(){
    m_sock = ::socket(AF_UNIX, SOCK_STREAM, 0);
    if (SOCKET_ERROR == ::connect(m_sock, reinterpret_cast<sockaddr*>(&m_remote), 
									     static_cast<socklen_t>(m_len))) {
		// TODO: logging is disabled to ensure beautiful output when running tests
		// std::cerr << "Connection failed. Error = " << errno << "\n";
		return false;
	}
    return true;
}

//TODO: no move here???
bool UnixSockClient::send(std::string&& msg) {
    std::string data = std::move(msg);
    data.append("\n");
	
    if (int bytes {SOCKET_ERROR}; m_sock > 0 && 
	    SOCKET_ERROR != (bytes = ::send(m_sock, data.data(), data.size(), MSG_NOSIGNAL))) {
        return true;
    }
    return false;
}

bool UnixSockClient::send(const std::string& msg) {
    std::string data {msg};
    data.append("\n");
	
    if (int bytes {SOCKET_ERROR}; m_sock > 0 && 
	    SOCKET_ERROR != (bytes = ::send(m_sock, data.data(), data.size(), MSG_NOSIGNAL))) {
        return true;
    }
    return false;
}