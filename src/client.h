#pragma once
 
#include <string>
#include <string_view>
#include <sys/un.h>

#define INVALID_SOCKET    -1
#define SOCKET_ERROR      -1

 
class UnixSockClient final{
public:
    explicit UnixSockClient(std::string_view sockPath);
    ~UnixSockClient();
 
    UnixSockClient(UnixSockClient &) = delete;
    UnixSockClient(UnixSockClient &&) = delete;
    UnixSockClient& operator=(UnixSockClient&) = delete;
    UnixSockClient& operator=(UnixSockClient&&) = delete;
 
    bool init();
	
    bool send(std::string&& msg);
	bool send(const std::string& msg);
	
	void close();
	
private:

    int m_sock {INVALID_SOCKET};
    sockaddr_un m_remote;
    size_t m_len {0};
};
