//============================================================================
// Name        : main.cpp
// Created on  : 30.05.2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : 
//============================================================================

#include <iostream> 
#include <atomic>  
#include <condition_variable>
#include <string>
#include <chrono>
#include <future>
#include <ostream>
#include <iomanip>
#include <random>

#include "client.h"
#include "server.h"
#include "ResourceMonitor.h"

namespace {
	// TODO: Redefine with appropriate and accessible file path
	constexpr std::string_view socket_path {"/tmp/handle.sock"};
}

namespace Color {
	
	enum class Code: uint16_t  {
		FG_BLACK   		 = 30,
        FG_RED      	 = 31,
        FG_GREEN   	 	 = 32,
		FG_YELLOW 		 = 33,
        FG_BLUE     	 = 34,
		FG_MAGENTA		 = 35,
		FG_CYAN 		 = 36,
		FG_LIGHT_GRAY 	 = 37,
        FG_DEFAULT  	 = 39,
        BG_RED      	 = 41,
        BG_GREEN    	 = 42,
        BG_BLUE     	 = 44,
        BG_DEFAULT  	 = 49,
		FG_DARK_GRAY     = 90,
		FG_LIGHT_RED 	 = 91,
		FG_LIGHT_GREEN   = 92,
		FG_LIGHT_YELLOW  = 93,
		FG_LIGHT_BLUE    = 94,
		FG_LIGHT_MAGENTA = 95,
		FG_LIGHT_CYAN 	 = 96,
		FG_WHITE		 = 97
    };
	
    class Modifier final {
	private:
		Code code {Code::FG_BLACK};
		
    public:
        Modifier(Code pCode) : code(pCode) {
		}
		
        friend std::ostream& operator<<(std::ostream& os, const Modifier& mod) {
            return os << "\033[" << static_cast<uint16_t>(mod.code) << "m";
        }
    };
}

//--------------------------------------------------------------------------------------------

// some
namespace {
	constexpr size_t TEST_NUM_CHAR_MAX { 5 };
	constexpr size_t TEST_NAME_CHAR_MAX { 60 };
	constexpr size_t TEST_RESULT_CHAR_MAX { 20 };
	
	using namespace Color;

	void print_tests_failed(size_t num) {
		std::cout << "Tests failed : " << Modifier(Code::FG_RED) 
				  << num << Modifier(Code::FG_DEFAULT) << std::endl;
	}
	
	void print_tests_passed(size_t num) {
		std::cout << "Tests passed : " << Modifier(Code::FG_GREEN) 
				  << num << Modifier(Code::FG_DEFAULT) << std::endl;
	}
	
	void print_tests_passed(bool result) {
		if (result) {
			std::cout << Modifier(Code::FG_GREEN) << "Passed";
		} else {
			std::cout << Modifier(Code::FG_RED) << "Failed";
		}
		std::cout << Modifier(Code::FG_DEFAULT) << std::endl;
	}
}

#define DEFINE_TEST_SUITE(class_name) { \
	class_name suite; \
	size_t testsRunned { 0 }; \
	size_t testsPassed { 0 }; \
	bool   result { false }; \
	std::cout << "\nRunning test suite: " << Modifier(Code::FG_LIGHT_CYAN) \
			  << #class_name << Modifier(Code::FG_DEFAULT) << "\n"; \
	std::cout << "-----------------------------------------------------------------------------\n"; \
	std::cout << std::setiosflags(std::ios::left) \
			  << std::setw(TEST_NUM_CHAR_MAX) << "#" \
		      << std::setw(TEST_NAME_CHAR_MAX) << "Name"  \
			  << std::setw(TEST_RESULT_CHAR_MAX) << "Result" << std::endl; \
	std::cout << "-----------------------------------------------------------------------------\n";


#define END_SUITE \
	std::cout << "-----------------------------------------------------------------------------\n"; \
	print_tests_passed(testsPassed); \
	print_tests_failed(testsRunned - testsPassed); \
	std::cout << "-----------------------------------------------------------------------------\n"; }


#define TEST(func_name) \
	std::cout << std::setiosflags(std::ios::left) \
			  << std::setw(TEST_NUM_CHAR_MAX) << ++testsRunned \
			  << std::setw(TEST_NAME_CHAR_MAX) << #func_name << std::flush; \
	result = suite.func_name(); \
	if (result) ++testsPassed; \
	print_tests_passed(result);
	
#define ASSERT_TRUE(condition) if (true != (condition)) \
	return false;
	
#define ASSERT_FASLE(condition) if (false != (condition)) \
	return false;


// Holds collection of methods, drivers and other supporting tools 
// required to automate test execution
class TestHarness {
private:
	UnixSockServer server;

	std::future<void> serverFuture{};
	std::vector<std::string> requests;

	mutable std::condition_variable cond_var;
	std::atomic<bool> serverReady{ false };

	inline static constexpr std::chrono::milliseconds SERVER_START_TIMEOUT {
			std::chrono::seconds(5) };

public:
	void startServer(size_t messagesExpected = 1) {
		// Clear received requests storage:
		requests.clear();
		// Set server initial state:
		serverReady.store(false);
		
		serverFuture = std::async([this, messagesExpected]() {
			const bool initialyzed = server.init(socket_path);
			serverReady.store(initialyzed);
			cond_var.notify_all();

			if (false == initialyzed)
				return;

			for (size_t count = 0; count < messagesExpected; ++count) {
				if (server.accepted()) {
					server.step([&](std::string&& request) {
						requests.push_back(request);
					});
				}
			}
			server.shutdown();
		});
	}

	void wait() {
		serverFuture.wait();
	}

	// TODO: refactor
	// TODO: timeout --> to seconds
	void setStepTimeout(size_t timeout) {
		server.setStepTimeout(timeout);
	}

	bool isServerStarted() const noexcept {
		std::mutex mtx;
		std::unique_lock<std::mutex> lock(mtx);

		// Wait for Server to start for preconfigured timeout and 
		// Return FALSE otherwise
		if (std::cv_status::timeout == cond_var.wait_for(lock, SERVER_START_TIMEOUT))
			return false; 
		
		return serverReady;
	}
	
	bool startServerAndWait(size_t messagesExpected = 1) {
		// TODO: 10 ??
		setStepTimeout(10);
		startServer(messagesExpected);
		return isServerStarted();
	}

	inline const std::vector<std::string>& getMessages() const noexcept {
		return requests;
	}
	
	std::string randomString(int size = 16) {
		std::random_device rd{};
		auto mtgen = std::mt19937 {rd()};
		auto ud = std::uniform_int_distribution<> {(int)'A', (int)'z'};

		std::string str;
		str.reserve(size);
		while (size-- > 0)
			str.push_back(static_cast<char>(ud(mtgen)));
		return str;
	}
};

/** Integration tests suite class implementaion : **/
class IntegrationTests final : public TestHarness {
public:
	bool shouldSuccessfullyHandleSingleMessage () {
		constexpr size_t count {1};
		startServerAndWait(count);
		
		UnixSockClient client(socket_path);
		ASSERT_TRUE(client.init());

		const std::string data {"Hello world!!!"};
		client.send(data);
		wait();
		
		ASSERT_TRUE(count == getMessages().size());
		ASSERT_TRUE(0 == data.compare(getMessages().front()));
		return true;
	}
	
	bool shouldSuccessfullyHandleManyMessages() {
		constexpr size_t count {100};
		startServerAndWait(count);
		
		UnixSockClient client(socket_path);
		ASSERT_TRUE(client.init());
		
		for (size_t i = 1; i <= count; ++i) {
			client.send("Request_" + std::to_string(i));
		}
		wait();
		
		ASSERT_TRUE(count == getMessages().size());
		return true;
	}
	
	bool shouldSuccessfullyHandleLongMessage() {
		startServerAndWait(1);
	
		UnixSockClient client(socket_path);
		ASSERT_TRUE(client.init());
		
		const std::string data = randomString(10240);
		client.send(data);
		wait();
		
		ASSERT_TRUE(1 == getMessages().size());
		ASSERT_TRUE(0 == data.compare(getMessages().front()));
		return true;
	}
	
	bool shouldSuccessfullyHandleEmptyMessage() {
		startServerAndWait(1);
	
		UnixSockClient client(socket_path);
		ASSERT_TRUE(client.init());
		
		const std::string data{};
		client.send(data);
		wait();
		
		ASSERT_TRUE(1 == getMessages().size());
		ASSERT_TRUE(0 == data.compare(getMessages().front()));
		return true;
	}
	
	bool clientShouldFailedToConnectIfServerIsDown() {
		UnixSockClient client(socket_path);
		
		ASSERT_FASLE(client.init());
		return true;
	}
	
	bool messagesOrderingMustBeHeld() {

		constexpr size_t count {100};
		std::vector<std::string> messages;
		messages.reserve(count);
		for (size_t i = 1; i <= count; ++i) {
			messages.push_back("Request_" + std::to_string(i));
		}

		startServerAndWait(messages.size());
		
		UnixSockClient client(socket_path);
		ASSERT_TRUE(client.init());
		
		std::for_each(messages.cbegin(), messages.cend(), [&](const auto& msg){
			client.send(msg);
		});
		wait();
		
		ASSERT_TRUE(count == getMessages().size());
		ASSERT_TRUE(messages == getMessages());
		return true;
	}

};

/** Performance tests suite class implementaion : **/
class PerformanceTests final : public TestHarness {
public:

	bool LoadTest() {
		constexpr size_t count {100'000};
		startServerAndWait(count);
		
		std::atomic<bool> loadDone { false };
		auto load = std::async([count, &loadDone]() {
			UnixSockClient client(socket_path);
			if (false == client.init())
				return;
			const std::string msg {"Request"};
			for (size_t i = 0; i< count; ++i) {
				client.send(msg);
				std::this_thread::sleep_for(std::chrono::nanoseconds(1));
			}
			loadDone.store(true);
		});
		
		
		auto statCollector = std::async([&loadDone]() {
			Utilities::SimpleResourceMonitor monitor;
			while (!loadDone) {
				monitor.readStats();
				// TODO: ******* Collect and analyze memory usage profile ****** 
				// monitor.getTotalMemory()
				// monitor.getAvailableMemory();
				// monitor.getFreeMemory();
				std::this_thread::sleep_for(std::chrono::milliseconds(250));
			}
		});	

		load.wait();
		statCollector.wait();
		wait();
		
		ASSERT_FASLE(Utilities::NetworkErrors().hasErrors());
		return true;
	}
};

int main(int argc, char** argumets) 
{
	DEFINE_TEST_SUITE(IntegrationTests);
		TEST(shouldSuccessfullyHandleSingleMessage);
		TEST(shouldSuccessfullyHandleManyMessages);
		TEST(shouldSuccessfullyHandleLongMessage);
		TEST(shouldSuccessfullyHandleEmptyMessage);
		TEST(clientShouldFailedToConnectIfServerIsDown);
		TEST(messagesOrderingMustBeHeld);
	END_SUITE;

	DEFINE_TEST_SUITE(PerformanceTests);
		TEST(LoadTest);
	END_SUITE;
	
	return 0;
}
