//============================================================================
// Name        : ResourceMonitor.cpp
// Created on  : May 30, 2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Simple resource monitor impl
//============================================================================

#include <iostream>
#include "ResourceMonitor.h"

namespace Utilities {

	std::string Base::exec(std::string&& command) const noexcept {
		FILE* pipe = popen(command.c_str(), "r");
		if (!pipe) {
			return {};
		}

		char buffer[128];
		std::string result {};
		while (!feof(pipe)) {
			if (nullptr != fgets(buffer, sizeof(buffer), pipe))
					result.append(buffer);
		}

		pclose(pipe);
		return result;
	}

	void Base::split(const std::string& str,
								      std::vector<std::string>& cont,
									  const std::string& delimiter) const noexcept {
		size_t pos = 0, prev = 0;
		while ((pos = str.find(delimiter, prev)) != std::string::npos) {
			cont.emplace_back(str.begin() + prev, str.begin() + pos);
			prev = pos + delimiter.length();
		}
		cont.emplace_back(str.begin() + prev, str.end());
	}
	
                     

	void SimpleResourceMonitor::parse_line(const std::string& line) noexcept {
		size_t pos = line.find(": ");
		if (std::string::npos == pos)
			return;

		auto entry = params.emplace(line.substr(0, pos), 0);
		pos = line.find_first_not_of(" ", pos + 1);
		if (std::string::npos == pos)
			return;

		const size_t pos1 = line.find(" ", pos);
		if (std::string::npos == pos1)
			return;

		// Using std::string_view more than x2 faster than std::string::substr()
		entry.first->second = 
			std::atol(std::string_view(line).substr(pos, pos1 - pos).data());
	}

	void SimpleResourceMonitor::readStats() noexcept {
		const std::string output { exec(CMD) };
		lines.clear();
		split(output, lines, "\n");

		params.clear();
		for (const auto& entry : lines)
			parse_line(entry);
	}

	inline size_t SimpleResourceMonitor::getValue(std::string&& name) noexcept {
		return params[name];
	}

	size_t SimpleResourceMonitor::getAvailableMemory() noexcept {
		return getValue("MemAvailable");
	}
	
	size_t SimpleResourceMonitor::getTotalMemory() noexcept {
		return getValue("MemTotal");
	}

	size_t SimpleResourceMonitor::getFreeMemory() noexcept {
		return getValue("MemFree");
	}


	bool NetworkErrors::hasErrors() noexcept  {
		const std::string output{ exec(CMD) };
		lines.clear();
		split(output, lines, "\n");

		for (const std::string& line : lines) {
			size_t pos = line.find(" ");
			if (std::string::npos == pos)
				break;
			int error =
				std::atoi(std::string_view(line).substr(0, pos).data());
			if (error > 0)
				return true;
		}
		return false;
	}
}
