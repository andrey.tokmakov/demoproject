//============================================================================
// Name        : ResourceMonitor.cpp
// Created on  : May 30, 2021
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Simple resource monitor definition
//============================================================================

#ifndef RESOURCEMONITOR_INCLUDE_GUARD__H
#define RESOURCEMONITOR_INCLUDE_GUARD__H

#include <string>
#include <vector>
#include <unordered_map>

namespace Utilities {
	
// Class defines the basic methods for working with system resources
class Base {
protected:
	std::vector<std::string> lines;
	
protected:
	std::string exec(std::string&& command) const noexcept;

	void split(const std::string& str,
			   std::vector<std::string>& cont,
			   const std::string& delimiter) const noexcept;
};

// class implements the functionality of getting statistics on 
// the state of the system's memory
class SimpleResourceMonitor final: public Base {
private:
	std::unordered_map<std::string, size_t> params;
	static inline constexpr char CMD[] = "cat /proc/meminfo";
	
protected:
	void parse_line(const std::string& line) noexcept;
	inline size_t getValue(std::string&& name) noexcept;

public:
	void readStats() noexcept;
	
	size_t getAvailableMemory() noexcept;
	size_t getTotalMemory() noexcept;
	size_t getFreeMemory() noexcept;
};


// Class is only used to check for network errors and it is probably 
// worth expanding it further
class NetworkErrors final : public Base {
private:
	static inline constexpr char CMD[] = "netstat -s | grep error";

public:
	bool hasErrors() noexcept;
};

}

#endif // !RESOURCEMONITOR_INCLUDE_GUARD__H
